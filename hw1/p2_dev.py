"""M345SC Homework 1, part 2
Name: Jingqi Yang
CID: 01525740
"""

import numpy as np
import matplotlib.pyplot as plt
import time
import random

def nsearch(L,P,target):
    """Input:
    L: list containing *N* sub-lists of length M. Each sub-list
    contains M numbers (floats or ints), and the first P elements
    of each sub-list can be assumed to have been sorted in
    ascending order (assume that P<M). L[i][:p] contains the sorted elements in
    the i+1th sub-list of L
    P: The first P elements in each sub-list of L are assumed
    to be sorted in ascending order
    target: the number to be searched for in L

    Output:
    Lout: A list consisting of Q 2-element sub-lists where Q is the number of
    times target occurs in L. Each sub-list should contain 1) the index of
    the sublist of L where target was found and 2) the index within the sublist
    where the target was found. So, Lout = [[0,5],[0,6],[1,3]] indicates
    that the target can be found at L[0][5],L[0][6],L[1][3]. If target
    is not found in L, simply return an empty list (as in the code below)
    """

    Lout=[]

    for i in range(len(L)):
        sorted = L[i][:P]
        unsorted = L[i][P:]

        found = False
        #binary search sorted parts
        start = 0
        end = P - 1

        while start <= end:
            mid = (start + end) // 2
            if sorted[mid] == target:
                found = True
                Lout.append([i, mid])
                #check if the adjacent numbers are also targets
                #since it's a sorted list the same numbers should be adjacent
                for index in range(mid - 1, start, -1):
                    if sorted[index] == target:
                        Lout.append([i, index])
                    else: break
                for index2 in range(mid + 1, end):
                    if sorted[index2] == target:
                        Lout.append([i, index2])
                    else: break
                break
            elif target < sorted[mid]:
                end = mid - 1
            else:
                start = mid + 1

        #check if the target is in the unsorted part
        for j in range(len(unsorted)):
            if unsorted[j] == target:
                Lout.append([i, P + j])

    return Lout


def nsearch_time():
    """Analyze the running time of nsearch.
    Add input/output as needed, add a call to this function below to generate
    the figures you are submitting with your codes.

    Discussion:
    My implementation of nsearch has 2 parts, for each sub-list of L, first is to
    use binary search to check the sorted part, and then just loop through the
    unsorted part to check if the target is in it or not. Since the targets may
    appear multiple times in the list, I also check the adjacent numbers in the
    binary search, and there is no break for the unsorted part.

    The runtime for the unsorted part is O(M-P) and it doesn't changing with the
    input. The runtime for the sorted part, the best case is O(1) such that the
    target is in the mid of the sorted part, and there is only 1 or 0 adjacent
    same number. The worst case is that all numbers in the sorted parts are targets,
    and essentially the runtime will be O(P) because we need to add all indexes
    pair to Lout. But the average runtime will be O(logP). The average runtime for
    nsearch is O(N(logP + (M-P))), the best case is O(N(1 + M - P)) and the worst
    case is O(NM).

    My algorithm is efficient as the binary search only takes O(logP) for the
    sorted part, and for the unsorted part, it takes at least (M-P)log(M-P) to
    sort it, or M-P to iterate it and arrange it as a dictionary. Directly iterating
    through it also takes O(M-P), so I consider using binary search and linear
    iteration seperately for the sorted and unsorted part efficient. 

    As mentioned above, the average runtime for nsearch is O(N(logP + (M-P)))
    fig1: the graph of increasing size of N versus the corresponding runtime of
    function nsearch, with fixed M=1000 and P=500. Overall the runtime increases
    linearly as M incrases linearly. There are some sudden increase and decrease
    in the graph, because all the lists are randomly generated, and the appearance
    of the target in those random lists also infect the runtime.
    fig2: the graph of increasing size of M versus the corresponding runtime of
    function nsearch, with fixed N=1000 and P=M//2 (half of the sublist is sorted).
    The runtime also increases linearly as M incrases linearly.
    fig3: the graph of increasing log(P) versus the corresponding runtime of
    function nsearch, with fixed N=1000 and M=500. I use semilogx here because as
    it's shown in the runtime, the runtime has log relationship with P. In the graph
    it shows that the runtime decreases when P increases, becase the sorted part
    increases and the unsorted part decrases which decrases the overall runtime.
    Note that since all data is generated randomly, the output of graphs vary each
    time when this function is called. The submitted graphs here are just examples
    of sample outputs.
    """

    #graph for increasing N
    N = []
    nt = []
    m = 1000
    target = random.randint(0, 1000)
    p = 500
    for n in range(1000, 10000, 500):
        N.append(n)
        L = generate(n, m, p)
        t1 = time.time()
        out = nsearch(L, p, target)
        t2 = time.time()
        nt.append(t2 - t1)

    plt.figure(1)
    plt.plot(N, nt)
    plt.xlabel('size of N')
    plt.ylabel('runtime for the given size of N')
    plt.title('Jingqi Yang, nsearch_time(), N vs. runtime with fixed M, P')

    #graph for increasing M
    M = []
    mt = []
    n = 1000
    for m in range(1000, 10000, 500):
        p = m // 2 #half of each sublist of length M is sorted
        M.append(m)
        L = generate(n, m, p)
        t1 = time.time()
        out = nsearch(L, p, target)
        t2 = time.time()
        mt.append(t2 - t1)

    plt.figure(2)
    plt.plot(M, mt)
    plt.xlabel('size of M')
    plt.ylabel('runtime for the given size of M')
    plt.title('Jingqi Yang, nsearch_time(), M vs. runtime with fixed N, P')

    #graph for increasing N
    P = []
    pt = []
    n = 1000
    m = 1000
    for p in range(100, 1000, 50):
        P.append(p)
        L = generate(n, m, p)
        t1 = time.time()
        out = nsearch(L, p, target)
        t2 = time.time()
        pt.append(t2 - t1)

    plt.figure(3)
    plt.semilogx(P, pt)
    plt.xlabel('log(P)')
    plt.ylabel('runtime for the given size of P')
    plt.title('Jingqi Yang, nsearch_time(), log(P) vs. runtime with fixed N, M')


def generate(n, m, p):
    """Generate a list L containing n sub-lists of length m, and
    the first p elements of each sub-list are sorted in ascending
    order.
    """
    L = []
    for i in range(n):
        plist = list(np.sort(random.sample(range(m), p)))
        unsorted = list(random.sample(range(m), m-p))
        L.append(plist + unsorted)
    return L

if __name__ == '__main__':

    #add call(s) to nsearch here which generate the figure(s)
    #you are submitting
    nsearch_time() #modify as needed
