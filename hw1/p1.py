"""M345SC Homework 1, part 1
Name: Jingqi Yang
CID: 01525740
"""

def ksearch(S,k,f,x):
    """
    Search for frequently-occurring k-mers within a DNA sequence
    and find the number of point-x mutations for each frequently
    occurring k-mer.
    Input:
    S: A string consisting of A,C,G, and Ts
    k: The size of k-mer to search for (an integer)
    f: frequency parameter -- the search should identify k-mers which
    occur at least f times in S
    x: the location within each frequently-occurring k-mer where
    point-x mutations will differ.

    Output:
    L1: list containing the strings corresponding to the frequently-occurring k-mers
    L2: list containing the locations of the frequent k-mers
    L3: list containing the number of point-x mutations for each k-mer in L1.

    Discussion:
    I first create a dictionary with each k-mer in the given S as the key, and
    the list of the indexes of the k-mer as the value. Then I iterate through this
    dictonary to build L1, L2 and L3. For L1 and L2, if the length of the index
    list >= frequency, the k-mer is a frequent k-mer, and I add the k-mer and its
    index list to L1 and L2 (key and value of the created dictionary). For L3, I
    add a nested for loop to iterate the dictionary again, and compare the 2 k-mers
    to see if it is a point-x mutation, and sum them up to form L3.

    The dictionary I created organizes the k-mers and it's easy to form L1 and L2
    by using this dictionary. It actually records the frequency of all the k-mers
    so it also helps to build L3 by just summing the frequencices up.

    Runtime:
    (1) Build the dictionary takes len(S) - k + 1 iterations, we say it's N.
    (2) The outer loop for dic is the length of the dic, M iterations.
    (3) The inner loop for dic is also of length M, M iterations.
    By (2) and (3), the nested for loop is M * M = M^2.
    C = N + M^2
    We suppose that S is really large, if the given S is formed by same repeated
    k-mer, then M will be really small M << N, (say M < 10), and the leading-order
    running time will be N, so the runtime is O(N). However, when M is approximately
    N or M=N, or M < N but M^2 > N (the square grows faster than linear), the
    leading-order running time will be M^2, so the runtime is O(M^2).
    """

    #dict[k-mer] = [indexes]
    dic = {}
    for i in range(len(S) - k + 1):
        currentMer = S[i:i + k]
        if currentMer in dic:
            dic[currentMer].append(i) #append the index
        else:
            dic[currentMer] = [i]

    L1,L2,L3=[],[],[]

    for mer in dic:
        indexL = dic[mer]
        if len(indexL) >= f: #compare the frequency
            L1.append(mer)
            L2.append(indexL)

            num = 0
            for mer2 in dic:
                if mer[x] != mer2[x]:
                    match = True
                    #using direct comparasion of strings
                    if mer[0:x] != mer2[0:x] or mer[x+1:k] != mer2[x+1:k]:
                        match = False
                    if match: num += len(dic[mer2])
            L3.append(num)

    return L1,L2,L3




if __name__=='__main__':
    #Sample input and function call. Use/modify if/as needed
    S='CCCTATGTTTGTGTGCACGGACACGTGCTAAAGCCAAATATTTTGCTAGT'
    #k=3
    #x=2
    #f=2
    #L1,L2,L3=ksearch(S,k,f,x):
